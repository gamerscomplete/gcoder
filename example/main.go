package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/gcoder"
	"math"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: go run filament_calc.go <path_to_gcode_file>")
		return
	}

	filePath := os.Args[1]

	gcode, err := gcoder.NewGCodeFromFile(filePath)
	if err != nil {
		log.Error("Failed to parse gcode file:", err)
		return
	}

	fmt.Printf("Total filament used: %.2fmm\n", gcode.GetTotalFilament())
	fmt.Println("Time: ", FormatTime(gcode.GetTotalTime()))
}

// FormatTime takes a float64 representing time in seconds and formats it into a string with hours, minutes, and seconds.
func FormatTime(totalSeconds float64) string {
	// Convert to an integer to avoid dealing with fractions of a second
	intSeconds := int(math.Round(totalSeconds))

	hours := intSeconds / 3600
	minutes := (intSeconds % 3600) / 60
	seconds := intSeconds % 60

	return fmt.Sprintf("%dH %dM %dS", hours, minutes, seconds)
}
