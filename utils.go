package gcoder

import (
	"math"
)

// Distance calculates the Euclidean distance between two Vector3 points.
func Distance(a, b Vector3) float64 {
	return math.Sqrt(math.Pow(b.X-a.X, 2) + math.Pow(b.Y-a.Y, 2) + math.Pow(b.Z-a.Z, 2))
}

type Vector3 struct {
	X, Y, Z float64
}
