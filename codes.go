package gcoder

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
)

/* --Gcodes--
M201 - Set Print Max Acceleration
This command sets the maximum acceleration for each axis in units of mm/s² (millimeters per second squared). It controls how quickly the printer can accelerate to its target speed for X, Y, Z, and E (extruder) movements.
Example: M201 X500 Y500 Z100 E10000 sets the maximum acceleration for the X and Y axes to 500 mm/s², Z to 100 mm/s², and the extruder to 10,000 mm/s².

M203 - Set Maximum Feedrate
M203 sets the maximum feedrate for each axis in mm/min (millimeters per minute). This command limits the speed at which the printer moves along each axis.
Example: M203 X5000 Y5000 Z200 E25 sets the maximum speeds for the X and Y axes to 5000 mm/min, Z to 200 mm/min, and the extruder to 25 mm/min.

M204 - Set Printing and Travel Accelerations
This command sets the acceleration for printing moves (P) and travel moves (T), again in mm/s². Printing moves are those involving extrusion, while travel moves do not.
Example: M204 P1500 T3000 sets the acceleration for printing moves to 1500 mm/s² and travel moves to 3000 mm/s².

M205 - Set Advanced Settings
M205 is used to set various advanced settings such as maximum jerk speeds and minimum segment times. It's often used to fine-tune the behavior of the printer for smoother movements and transitions.
Example: M205 X10.00 Y10.00 Z0.40 E1.00 might set the maximum jerk for X and Y axes and other specific behaviors.

G90 - Set to Absolute Positioning
G90 switches the printer's movement mode to absolute positioning, meaning all coordinate movements will be from a fixed origin point of the printer's build area.
Example: G90


M83 - Set Extruder to Relative Mode
M83 sets the extruder to operate in relative mode, meaning all E commands will move the extruder a relative amount from its current position, rather than from an absolute position.
Example: M83

M104 - Set Extruder Temperature
M104 sets the temperature of the extruder to the specified value in degrees Celsius without waiting for the temperature to stabilize.
Example: M104 S210 sets the extruder temperature to 210°C.

M140 - Set Bed Temperature
This command sets the bed temperature to the specified value in degrees Celsius without waiting for the temperature to reach that value.
Example: M140 S60 sets the bed temperature to 60°C.

G4 - Dwell
G4 pauses the printer for a given amount of time. This can be useful for waiting for temperature changes or other operations to complete.
Example: G4 S10 pauses the printer for 10 seconds.

G28 - Auto Home
G28 moves the printer's head to the origin (home) position of each axis, ensuring it knows its precise location.
Example: G28 homes all axes.

M190 - Wait for Bed Temperature
M190 sets the bed temperature and waits until the bed reaches the specified temperature before continuing.
Example: M190 S60 waits for the bed to reach 60°C.

G92 - Set Position
G92 sets the current position to the coordinates given. This can be used to reset the position on one or more axes without moving the tool.
Example: G92 E0 resets the extruder's position to 0.

M107 - Turn Fan Off
M107 turns off the cooling fan.
Example: M107 stops the fan.


*/

type Code interface {
	GcodeString() string
}

type Unsupported struct {
	Line string
}

func (u *Unsupported) GcodeString() string {
	return ""
}

// Linear move
type G1 struct {
	//x,y coordinate
	X *float64
	Y *float64
	Z *float64
	//extrude amount
	E *float64
	//feedrate
	F *int
}

func (g *G1) GcodeString() string {
	var parts []string
	parts = append(parts, "G1") // Command identifier

	if g.X != nil {
		parts = append(parts, fmt.Sprintf("X%.2f", *g.X))
	}
	if g.Y != nil {
		parts = append(parts, fmt.Sprintf("Y%.2f", *g.Y))
	}
	if g.Z != nil {
		parts = append(parts, fmt.Sprintf("Z%.2f", *g.Z))
	}
	if g.E != nil {
		parts = append(parts, fmt.Sprintf("E%d", *g.E))
	}
	if g.F != nil {
		parts = append(parts, fmt.Sprintf("F%d", *g.F))
	}

	return strings.Join(parts, " ")
}

func NewG1(fields []string) *G1 {
	g1 := G1{}

	for _, part := range fields {
		switch string(part[0]) {
		case "X":
			x, err := strconv.ParseFloat(part[1:], 64)
			if err != nil {
				log.Error("Error parsing X value: ", err)
				continue
			}
			g1.X = &x
		case "Y":
			y, err := strconv.ParseFloat(part[1:], 64)
			if err != nil {
				log.Error("Error parsing Y value: ", err)
				continue
			}
			g1.Y = &y
		case "Z":
			z, err := strconv.ParseFloat(part[1:], 64)
			if err != nil {
				log.Error("Error parsing Z value: ", err)
				continue
			}
			g1.Z = &z
		case "E":
			extrusion, err := strconv.ParseFloat(part[1:], 64)
			if err != nil {
				log.Error("Error parsing extrusion value: ", err)
				continue
			}
			g1.E = &extrusion
		case "F":
			feedrate, err := strconv.Atoi(part[1:])
			if err != nil {
				log.Error("Error parsing feedrate value: ", err)
				continue
			}
			g1.F = &feedrate
		default:
			log.Warn("Unknown field type in G1 type: ", string(part[0]), " for line: ", strings.Join(fields, " "))
		}
	}
	return &g1
}
