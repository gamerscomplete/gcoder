package gcoder

import (
	"bufio"
	"io"
	"os"
	"strings"
)

type GCode struct {
	Name  string
	Codes []Code
}

func NewGCodeFromFile(filename string) (*GCode, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return NewGcode(file)
}

func NewGcodeFromString(input string) (*GCode, error) {
	reader := strings.NewReader(input)
	return NewGcode(reader)
}

func NewGcode(reader io.Reader) (*GCode, error) {
	scanner := bufio.NewScanner(reader)
	gcode := GCode{Name: ""}

	for scanner.Scan() {
		line := scanner.Text()

		fields := strings.Fields(stripComments(line))
		//Check if this line is empty
		if len(fields) < 1 {
			continue
		}
		//Check if this line is a comment
		if strings.HasPrefix(line, ";") {
			continue
		}
		gcode.Codes = append(gcode.Codes, GetGcode(fields))
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return &gcode, nil

}

func GetGcode(fields []string) Code {
	switch fields[0] {
	case "G1":
		return NewG1(fields[1:])
	default:
		return &Unsupported{Line: strings.Join(fields, " ")}
	}
}

func stripComments(s string) string {
	parts := strings.Split(s, ";")
	return strings.TrimSpace(parts[0])
}

func (gcode *GCode) GetTotalTime() float64 {
	var totalTime float64
	var curVec Vector3
	var feedrate int

	for _, v := range gcode.Codes {
		if g1, ok := v.(*G1); ok {
			var newVec Vector3
			var moved bool
			if g1.F != nil {
				feedrate = *g1.F
			}
			if g1.X != nil {
				newVec.X = *g1.X
				moved = true
			} else {
				newVec.X = curVec.X
			}
			if g1.Y != nil {
				newVec.Y = *g1.Y
				moved = true
			} else {
				newVec.Y = curVec.Y
			}
			if g1.Z != nil {
				newVec.Z = *g1.Z
				moved = true
			} else {
				newVec.Z = curVec.Z
			}

			if moved {
				//calculate the distance from previous vector3 to new vector3
				distance := Distance(curVec, newVec)
				totalTime += (distance / float64(feedrate)) * 60
				curVec = newVec
			}
		}
	}

	return totalTime
}

func (gcode *GCode) GetTotalFilament() float64 {
	var totalExtrusion float64
	for _, v := range gcode.Codes {
		if g1, ok := v.(*G1); ok {
			if g1.E != nil {
				totalExtrusion += *g1.E
			}
		}
	}
	return totalExtrusion
}
